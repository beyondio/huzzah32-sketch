#include <main.h>

void setup() {
  //SERIAL COMMUNICATION
  Serial.begin(115200);
  Serial.println("Starting Etch-a-sketch...");

    // PINS SETUP
  for (int i = 0; i < numBut;i++) {
    pinMode(buttons[i], INPUT_PULLUP);
  }
  pinMode(ledPin, OUTPUT);

  //WIFI
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid); // SSID = name of the network you are connecting to.
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  WiFi.setSleep(false);

  while (WiFi .status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");     
  }
  Serial.println("");

  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  digitalWrite(ledPin, HIGH); //turn on the LED when we're connected to wifi.
  
  //ACCELEROMETER
  Serial.println();
  Serial.println("LIS3DH test!");
  
  if (! lis.begin(0x18)) {   // change this to 0x19 for alternative i2c address
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("LIS3DH found!");
  
  lis.setRange(LIS3DH_RANGE_2_G);   // 2, 4, 8 or 16 G!
  
  Serial.print("Range = "); Serial.print(2 << lis.getRange());  
  Serial.println("G");
}

void loop() {
  checkPots();
  checkButtons();
  checkAccelerometer(); 
  checkShake();

  delay(16);
}

void checkPots() {
  for (int i=0; i<numPot; i++){
        int potValue = analogRead(pots[i]);

        //send OSC message
        OSCMessage msg("/pots/p");
        msg.add(i);
        msg.add(potValue);

        Udp.beginPacket(outIp, outPort);
        msg.send(Udp);
        Udp.endPacket();
        
        msg.empty();
  }
}

void checkButtons() {
  //arrays to hold current and last state of buttons 
  static byte lastState[numBut];
  static byte currentState[numBut];

  //start a for loop
  for (int i=0; i<numBut; i++){
    // read the button pins and fill current array
    currentState[i] = digitalRead(buttons[i]);
    
    //compare each index of the current and last array
    if (currentState[i] != lastState[i]){  //if they are not the same as last time     
        //send OSC message
        if(currentState[i] == 0) {
          OSCMessage msg("/buttons/b");
          msg.add(i);
          msg.add(1);

          Udp.beginPacket(outIp, outPort);
          msg.send(Udp);
          Udp.endPacket();

          msg.empty();
        }
        
        //overwrite the last state with the current one
        lastState[i] = currentState[i];
    }
  }
}
void checkAccelerometer() {
  lis.read(); // get X Y and Z data at once

  //send OSC message
  OSCMessage msg("/acc");
  msg.add(lis.x);
  msg.add(lis.y);
  msg.add(lis.z);

  Udp.beginPacket(outIp, outPort);
  msg.send(Udp);
  Udp.endPacket();

  msg.empty();
}

void checkShake() {
  // Shake detection
  // ====================================================
  // Take an average of 10 read-outs and compare to threshold
  accX = 0;
  accY = 0;
  accZ = 0;
  
  for (int i=0; i<10; i++) {
    lis.read();
    accX += lis.x;
    accY += lis.y;
    accZ += lis.z;
  }
  
  accX /= 10;
  accY /= 10;
  accZ /= 10;
 
  totalAccel = sqrt(accX*accX + accY*accY + accZ*accZ);
 
  if (totalAccel > SHAKE_THRESHOLD) {
    Serial.println(shakeCounter);
    shakeCounter += 1;
  
    if (shakeCounter == SHAKE_TRIGGER) {
      Serial.println("SHAKE IT LIKE A POLAROID PICTURE!");
      Serial.println();
      shakeCounter = 0;

      OSCMessage msg("/acc/shake");
      msg.add(1);

      Udp.beginPacket(outIp, outPort);
      msg.send(Udp);
      Udp.endPacket();

      msg.empty();
    }
  }
}