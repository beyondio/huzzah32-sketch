// LIBS
// ====================================================
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <OSCBundle.h>
#include <Adafruit_LIS3DH.h>

// METHODS
// ====================================================
void checkPots();
void checkButtons();
void checkAccelerometer();
void checkShake();

byte ledPin = 13;

//button pin array, add the pins your buttons are on
byte buttons[] = {15,32,14};
const int numBut = sizeof(buttons);

//fader pin array, add the pins your sensors are on
byte pots[] = {A2,A3,A4,A9};
const int numPot = sizeof(pots);

// WIFI & OSC
// ====================================================
char ssid[] = "imagining_code";               // your network SSID (name)
char pass[] = "codecode";     // your network password

WiFiUDP Udp; // A UDP instance to let us send and receive packets over UDP
const IPAddress outIp(192, 168, 100, 101); // remote IP of your computer
const unsigned int outPort = 8000; // remote port to receive OSC
const unsigned int localPort = 9000;

OSCBundle bundle;

// ACCELEROMETER TYPE LIS3DH
// ====================================================
#define LIS3DH_CLK 5
#define LIS3DH_MISO 19
#define LIS3DH_MOSI 18
#define LIS3DH_CS 4

Adafruit_LIS3DH lis = Adafruit_LIS3DH(LIS3DH_CS);

// Shake detection
// ---------------------------------------------------
#define SHAKE_THRESHOLD 40000 // Total acceleration threshold for roll detect
#define SHAKE_TRIGGER 5 // Number of shakes we need to detect before we send a value
float accX, accY, accZ, totalAccel;
int shakeCounter = 0;